﻿using System;
using Android.App;
using Android.OS;
using Android.Widget;
using Com.Wrappers.Xappmedia;
using Java.Lang;
using Android.Util;
using Android.Support.V7.App;

namespace Test
{
    [Activity(Label = "Test", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : ActionBarActivity, IAdPlayerWrapperListener
    {
        private Button playButton;
        private Button stopButton;
        private Button pauseButton;
        private Button resumeButton;
        private AudioOnly sdk;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);

            playButton = FindViewById<Button>(Resource.Id.playNewAd);
            stopButton = FindViewById<Button>(Resource.Id.stopAd);
            pauseButton = FindViewById<Button>(Resource.Id.pauseAd);
            resumeButton = FindViewById<Button>(Resource.Id.resumeAd);

            playButton.Click += PlayButton_Click;
            stopButton.Click += StopButton_Click;
            pauseButton.Click += PauseButton_Click;
            resumeButton.Click += ResumeButton_Click;

            sdk = new AudioOnly(this, this);
            sdk.LoadInterstitial(this);
        }

        private void ReCreateAudioOnly()
        {
            if (sdk != null)
            {
                sdk.Destroy();
            }
            sdk = new AudioOnly(ApplicationContext, this);
            sdk.LoadNewAd(ApplicationContext);
        }

        private void ResumeButton_Click(object sender, EventArgs e)
        {
            sdk.Resume();
        }

        private void PauseButton_Click(object sender, EventArgs e)
        {
            sdk.Pause();
        }

        private void StopButton_Click(object sender, EventArgs e)
        {
            sdk.Stop();
        }

        private void PlayButton_Click(object sender, EventArgs e)
        {
            ReCreateAudioOnly();
        }

        public void OnError(Throwable error)
        {
            Log.Error("XAPP", "ups!, something happened");
            Log.Error("XAPP", error.Message);
            var exception = error.InnerException;
            while (exception != null)
            {
                Log.Error("XAPP", exception.Message);
                exception = exception.InnerException;
            }
        }

        public void OnPlaybackChanged(PlaybackStatus status)
        {
            Log.Info("XAPP", $"Status changed to '{status}'");
        }

        public void OnReady()
        {
            Log.Info("XAPP", "AudioOnly instance is ready");
        }

        public void OnNewAdLoaded(XappInformation adInformation)
        {
            Log.Info("XAPP",
                $"Ad '({adInformation.Id}) {adInformation.CampaignName} {adInformation.Title} ({adInformation.Type})' is loaded. (account: {adInformation.AccountName})");
            sdk.Play();
        }
    }
}