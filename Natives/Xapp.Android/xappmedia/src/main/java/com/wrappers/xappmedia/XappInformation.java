package com.wrappers.xappmedia;

import xappmedia.sdk.model.Advertisement;

/**
 * Created by fcall on 2/25/2017.
 */

public class XappInformation {
    private Advertisement xappAd;

    public XappInformation(Advertisement ad){
        xappAd = ad;
    }

    public String getId(){
        return xappAd.adId();
    }

    public String getType(){
        return xappAd.adType().name();
    }

    public String getAccountName(){
        return xappAd.accountName();
    }

    public String getCampaignName(){
        return xappAd.campaignName();
    }

    public String getTitle(){
        return xappAd.adTitle();
    }
}
