package com.wrappers.xappmedia;

import android.app.Activity;
import android.content.Context;

/**
 * Created by fcall on 2/23/2017.
 *
 * We need this class to avoid any direct reference to Xapp classes
 * Otherwise Xamarin Jar Parser won't be able to create the
 * classes needed to to the bindings
 *
 * Essentially we need to completely hide Xapp from Xamarin
 */
public class AudioOnly {
    private AdPlayerWrapper adPlayer;
    private IAdPlayerWrapperListener wrapperListener;

    public AudioOnly(Context context, IAdPlayerWrapperListener listener){
        wrapperListener = listener;
        adPlayer = new AdPlayerWrapper(wrapperListener);
    }

    public boolean getIsPlaying(){
        if (adPlayer == null) {
            return false;
        }
        return adPlayer.getIsPlaying();
    }

    public boolean getIsLoadingAd(){
        if (adPlayer == null) {
            return false;
        }
        return adPlayer.getIsLoadingAd();
    }

    public void loadInterstitial (Activity activity){
        if (adPlayer == null) {
            return;
        }
        adPlayer.loadNewInterstitial(activity);
    }

    public void loadNewAd (Context context){
        if (adPlayer == null) {
            return;
        }
        adPlayer.loadNewAd(context);
    }

    public void play (){
        if (adPlayer == null) {
            return;
        }
        adPlayer.start();
    }

    public void stop (){
        if (adPlayer == null) {
            return;
        }

        adPlayer.stop();
    }

    public void pause (){
        if (adPlayer == null) {
            return;
        }
        adPlayer.pause();
    }

    public void resume (){
        if (adPlayer == null) {
            return;
        }
        adPlayer.resume();
    }

    public void destroy(){
        if (adPlayer != null) {
            adPlayer.clearController();
            adPlayer = null;
        }
    }
}