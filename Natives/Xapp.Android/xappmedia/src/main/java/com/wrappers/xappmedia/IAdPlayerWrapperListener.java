package com.wrappers.xappmedia;

import android.support.annotation.NonNull;

/**
 * Created by fcallejon on 24/02/2017.
 */

public interface IAdPlayerWrapperListener {
    void onNewAdLoaded(@NonNull XappInformation ad);

    void onReady();

    void onError(@NonNull Throwable throwable);

    void onPlaybackChanged(@NonNull PlaybackStatus status);
}
