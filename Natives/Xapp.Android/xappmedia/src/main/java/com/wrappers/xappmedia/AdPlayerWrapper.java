package com.wrappers.xappmedia;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import xappmedia.sdk.LoadCallback;
import xappmedia.sdk.PlayListener;
import xappmedia.sdk.Xapp;
import xappmedia.sdk.XappAdController;
import xappmedia.sdk.model.AdResult;

/**
 * Created by fcall on 2/24/2017.
 */

class AdPlayerWrapper implements PlayListener, LoadCallback {
    private XappAdController xappController;
    private final String TAG = "XMWRAPPER";
    private IAdPlayerWrapperListener wrapperListener;
    private boolean isLoadingAd;

    public AdPlayerWrapper(IAdPlayerWrapperListener listener) {
        wrapperListener = listener;
        Log.d(TAG, "AdPlayerWrapper initialized");
        isLoadingAd = false;
    }

    public boolean getIsLoadingAd(){
        return isLoadingAd;
    }

    public boolean getIsPlaying(){
        return xappController != null && xappController.isPlaying();
    }

    public void loadNewInterstitial(Activity activity){
        if (isLoadingAd){
            return;
        }
        isLoadingAd = true;

        xappController = Xapp.from(activity)
                .xappAds()
                .newRequest()
                .loadAd(activity, 0, this);

        initController();
        Log.d(TAG, "AdPlayerWrapper re-instantiated");
    }

    public void loadNewAd(Context context){
        if (isLoadingAd){
            return;
        }
        isLoadingAd = true;

        xappController = Xapp.from(context)
                .xappAds()
                .newRequest()
                .loadAd(this);
        initController();
        Log.d(TAG, "AdPlayerWrapper re-instantiated");
    }

    public void start(){
        if (xappController == null ||
                xappController.currentAd() == null){
            return;
        }

        xappController.start();
        Log.d(TAG, "Started playing Ad... " + xappController.currentAd().adTitle());
    }

    public void stop(){
        xappController.stop();
    }

    public void pause(){
        xappController.pause();
    }

    public void resume(){
        xappController.start();
    }

    public void clearController() {
        if (xappController == null){
            return;
        }
        xappController.stop();
        xappController.setPlayListener(null);
        xappController.release();
        xappController = null;
    }

    private void initController() {
        if (xappController != null) {
            xappController.setPlayListener(this);

            if (wrapperListener != null){
                wrapperListener.onReady();
            }

            return;
        }

        throw new NullPointerException("xappController is null");
    }

    @Override
    public void onLoad(@NonNull xappmedia.sdk.model.Advertisement advertisement) {
        isLoadingAd = false;
        wrapperListener.onNewAdLoaded(new XappInformation(advertisement));
    }

    @Override
    public void onError(@NonNull Throwable throwable) {
        isLoadingAd = false;
        if (wrapperListener != null){
            wrapperListener.onError(throwable);
        }
        Log.e(TAG, "AdPlayerWrapper Ups!", throwable);
    }

    @Override
    public void onStart(xappmedia.sdk.model.Advertisement advertisement) {
        if (wrapperListener != null){
            wrapperListener.onPlaybackChanged(PlaybackStatus.Playing);
        }
        Log.d(TAG, "AdPlayerWrapper started playing " + advertisement.adTitle());
    }

    @Override
    public void onPause(xappmedia.sdk.model.Advertisement advertisement) {
        if (wrapperListener != null){
            wrapperListener.onPlaybackChanged(PlaybackStatus.Paused);
        }
        Log.d(TAG, "AdPlayerWrapper paused playing of " + advertisement.adTitle());
    }

    @Override
    public void onStop(xappmedia.sdk.model.Advertisement advertisement) {
        if (wrapperListener != null){
            wrapperListener.onPlaybackChanged(PlaybackStatus.Stopped);
        }
        Log.d(TAG, "AdPlayerWrapper stopped playing of " + advertisement.adTitle());
    }

    @Override
    public void onFinish(@Nullable xappmedia.sdk.model.Advertisement advertisement, @NonNull AdResult adResult) {
        if (wrapperListener != null){
            wrapperListener.onPlaybackChanged(PlaybackStatus.ReachedEnd);
        }

        Log.d(TAG, "AdPlayerWrapper finished playing " + xappController.currentAd().adTitle());
        Log.d(TAG, "AdPlayerWrapper ad playing result ok: " + adResult.isSuccess());
        Log.d(TAG, "AdPlayerWrapper ad playing phrase: " + adResult.getActionPhrase());
        if (adResult.hasError()){
            Log.e(TAG, "AdPlayerWrapper ad playing ended with error" + adResult.getErrorCode() + " .\n\r" + adResult.getErrorMessage());
        }
        clearController();
    }
}