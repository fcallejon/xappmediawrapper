package com.wrappers.xappmedia;

/**
 * Created by fcallejon on 24/02/2017.
 */

public enum PlaybackStatus {
    Playing,
    Stopped,
    Paused,
    ReachedEnd
}
